<?php

//Import the constants from another file for security
require_once('secure/constants.php');

$action = $_GET['action'];
$col = $_GET['col'];
$ref = $_GET['ref'];
$id = $_GET['id'];
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////

if(isset($action) && $action == "update") {
	if(isset($action) && $col == "SUMMARY"){	
	
	$id = $_GET['id'];
	$summ = $_POST['summary'];
	update_data($id, $summ, $col);
	}
	
	else if(isset($action) && $col == "EDUCATION"){
		$id = $_GET['id'];
		$educ = $_POST['education'];
		update_data($id, $educ, $col);		
	}
	
	else if(isset($action) && $col == "CURRENTLY"){
		$id = $_GET['id'];
		$curr = $_POST['currently'];
		update_data($id, $curr, $col);		
	}
	
	else if(isset($action) && $col == "USERNAME"){
		$id = $_GET['id'];
		$usrname = $_POST['username'];
		update_data($id, $usrname, $col);		
	}

		else if(isset($action) && $col == "PASSWORD"){
		$id = $_GET['id'];
		$passw = $_POST['password'];
		update_data($id, $passw, $col);		
	}
	else if(isset($action) && $col == "FNAME"){
		$id = $_GET['id'];
		$finame = $_POST['fname'];
		update_data($id, $finame, $col);		
	}	

	else if(isset($action) && $col == "LNAME"){
		$id = $_GET['id'];
		$laname = $_POST['lname'];
		update_data($id, $laname, $col);		
	}	
	
		else if(isset($action) && $col == "ADDRESS"){
		$id = $_GET['id'];
		$adrss = $_POST['address'];
		update_data($id, $adrss, $col);		
	}		
	
		else if(isset($action) && $col == "JOB"){
		$id = $_GET['id'];
		$jobtt = $_POST['job'];
		update_data($id, $jobtt, $col);		
	}			

		else if(isset($action) && $col == "EMAIL"){
		$id = $_GET['id'];
		$e_mail = $_POST['email'];
		update_data($id, $e_mail, $col);		
	}	
	
		else if(isset($action) && $col == "PHONE"){
		$id = $_GET['id'];
		$phno = $_POST['phone'];
		update_data($id, $phno, $col);		
	}	

		else if(isset($action) && $col == "CELL"){
		$id = $_GET['id'];
		$cellno = $_POST['cell'];
		update_data($id, $cellno, $col);		
	}	

		else if(isset($action) && $col == "PROF_IMG"){
		$id = $_GET['id'];
		$prof_img = $_POST['prof_img'];
		update_data($id, $prof_img, $col);		
	}	
		else if(isset($action) && $col == "INT_IMG01"){
		$id = $_GET['id'];
		$int_img01 = $_POST['int_img01'];
		update_data($id, $int_img01, $col);		
	}		

		else if(isset($action) && $col == "INT_IMG02"){
		$id = $_GET['id'];
		$int_img02 = $_POST['int_img02'];
		update_data($id, $int_img02, $col);		
	}		

		else if(isset($action) && $col == "INT_IMG03"){
		$id = $_GET['id'];
		$int_img03 = $_POST['int_img03'];
		update_data($id, $int_img03, $col);		
	}	

		else if(isset($action) && $col == "INT_IMG04"){
		$id = $_GET['id'];
		$int_img04 = $_POST['int_img04'];
		update_data($id, $int_img04, $col);		
	}	

		else if(isset($action) && $col == "INT_IMG05"){
		$id = $_GET['id'];
		$int_img05 = $_POST['int_img05'];
		update_data($id, $int_img05, $col);		
	}	
	
		else if(isset($action) && $col == "INT_IMG06"){
		$id = $_GET['id'];
		$int_img06 = $_POST['int_img06'];
		update_data($id, $int_img06, $col);		
	}	
	
		else if(isset($action) && $col == "INT_IMG07"){
		$id = $_GET['id'];
		$int_img07 = $_POST['int_img07'];
		update_data($id, $int_img07, $col);		
	}	
	
		else if(isset($action) && $col == "INT_IMG08"){
		$id = $_GET['id'];
		$int_img08 = $_POST['int_img08'];
		update_data($id, $int_img08, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT01"){
		$id = $_GET['id'];
		$int_txt01 = $_POST['int_txt01'];
		update_data($id, $int_txt01, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT02"){
		$id = $_GET['id'];
		$int_txt02 = $_POST['int_txt02'];
		update_data($id, $int_txt02, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT03"){
		$id = $_GET['id'];
		$int_txt03 = $_POST['int_txt03'];
		update_data($id, $int_txt03, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT04"){
		$id = $_GET['id'];
		$int_txt04 = $_POST['int_txt04'];
		update_data($id, $int_txt04, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT05"){
		$id = $_GET['id'];
		$int_txt05 = $_POST['int_txt05'];
		update_data($id, $int_txt05, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT06"){
		$id = $_GET['id'];
		$int_txt06 = $_POST['int_txt06'];
		update_data($id, $int_txt06, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT07"){
		$id = $_GET['id'];
		$int_txt07 = $_POST['int_txt07'];
		update_data($id, $int_txt07, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT08"){
		$id = $_GET['id'];
		$int_txt08 = $_POST['int_txt08'];
		update_data($id, $int_txt08, $col);		
	}	
}



else {
	$action = 0;
}

////////////////////////////////////
////////////////////////////////////
////////////////////////////////////

//Create functions that connects us to the database
function connection() {
		
	$conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
	
	if($conn->connect_errno > 0){
    	die('Unable to connect to database [' . $conn->connect_error . ']');
	}
	
	return $conn;
}

////////////////////////////////////
////////////////////////////////////
////////////////////////////////////

// 	function uploadToDB($id, $opt, $col) {
		
// 		$db = connection();
// 		$newname = getFileName($name);
// 		$sql = "SELECT IMAGE FROM MyOdysseyUsers WHERE IMAGE = '$newname'";
// 		$result = $db->query($sql);
// 		$count = $db->affected_rows;
		
// 		if($count <= 0) {	
// 			$sql2 = "INSERT INTO MyOdysseyUsers (NAME, IMAGE) VALUES ('$name', '$newname')";
// 			$result2 = $db->query($sql2);
// 			$count2 = $db->affected_rows;
// 			if($count2 == 1) {
// 				uploadFile($newname); //Only run function when the db has accepted the entry
// 			}	
// 		}
// 		else {
// 			//Need some message to say that the file has already been taken
// 			header('Location: //.php');
// 		}
// 	}
	
// 	function getFileName($name) {
// 		$info = pathinfo($_FILES['userFile']['name']);
// 		$ext = $info['extension']; // get the extension of the file
// 		$newname = $name.".".$ext;
		
// 		return $newname;
// 	}
	
// 	function uploadFile($name) {
// 		$target = 'images/'.$name; //change if your images folder is not 'images'
// 		move_uploaded_file( $_FILES['userFile']['tmp_name'], $target);
		
// 			header('Location: ../../profile.php?id='.$id);
// 	}





function update_data($id, $opt, $col) {
	
	$db = connection();
	
	$id			=	$db->real_escape_string($id);
	$opt		=	$db->real_escape_string($opt);
	$col		=	$db->real_escape_string($col);
	
	var_dump ($id);
	
	$sql = "UPDATE MyOdysseyUsers SET $col = '$opt' WHERE ID = ".$id;
	
	$result = $db->query($sql);
	
	$count = $db->affected_rows;
	
	if(!$result){
		print_r($sql);
		echo "<br><br>";
		print_r($id);
		echo "<br><br>";
		print_r($opt);
		echo "<br><br>";		
		print_r($col);
		echo "<br><br>";		
		echo $count;
		die($db->error);	
	}
	
	if($count == 1) {
		header('Location: ../../profile.php?id='.$id);
// 		header('Location: ../$ref');
	}
	
	//Insert function does not return anything, but it can give us a count of the number of records added, which should be one
	//When that happens we will be redirected to the homepage
}


////////////////////////////////////
////////////////////////////////////
////////////////////////////////////



?>